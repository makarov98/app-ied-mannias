//
//  MappaInterattivaController.swift
//  UmarellApp
//
//  Created by iedstudent on 20/01/2020.
//  Copyright © 2020 IED. All rights reserved.
//

import UIKit
import MapKit

class MappaInterattivaController: UIViewController {
    let manager = CLLocationManager()
    
    // MARK: - Variabili Locali
    
    var eventoDaCreare: CreaEvento?
    
    // MARK: - Outlets
    
    @IBOutlet weak var MapView: MKMapView!
    
    // MARK: - Setup
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // chiedo la posizione dell'utente al sistema operativo
        manager.requestWhenInUseAuthorization()
        
        // mostro la mia posizione sulla mappa
        MapView.showsUserLocation = true
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapRecognizer(_:)))
        MapView.addGestureRecognizer(tap)
    }
    
    // Funzione che viene richiamata quando viene tappato un punto sulla MapView
    @objc func tapRecognizer(_ sender: UITapGestureRecognizer) {
    
        // Prendo il punto X e Y toccato dall'utente
        let puntoTappato = sender.location(in: sender.view)
    
        // Chiedo alla MapView di darmi le coordinate geografiche in quel punto X e Y
        let coordinateTappate = MapView.convert(puntoTappato, toCoordinateFrom: MapView)
        print(coordinateTappate)
    
        // Converto le coordinate in indirizzo (chiamata asincrona)
        Locationutility.indirizzo(con: coordinateTappate) {
        (indirizzoTrovato) in
    
        print(indirizzoTrovato ?? "Indirizzo non valido")
        }
    }
}

