//
//  CreaEventoController1.swift
//  UmarellApp
//
//  Created by iedstudent on 20/01/2020.
//  Copyright © 2020 IED. All rights reserved.
//

import UIKit

class CreaEventoController1 : UIViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var textNome: UITextField!
    
    @IBOutlet weak var textData: UITextField!
    
    @IBOutlet weak var textPrezzo: UITextField!
    
    // MARK: - Variabili locali
    
    // MARK: - Setup
    
    var eventoDaCreare = CreaEvento ()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let datePicker = UIDatePicker()
        datePicker.addTarget (self, action: #selector(datePickerModificato(_ :)),
        for: .valueChanged)
        textData.inputView = UIDatePicker()
    }
    
    private func popolaEventoDaCreare() {
        eventoDaCreare.nome = textNome.text
        
        if let stringa = textPrezzo.text, let double = Double(stringa){
            eventoDaCreare.prezzo = double
        }
        
        if let datePicker = textData.inputView as? UIDatePicker {
            eventoDaCreare.data = datePicker.date
        }
    }
}
