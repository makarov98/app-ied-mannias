//
//  CreaEventoController2.swift
//  UmarellApp
//
//  Created by iedstudent on 20/01/2020.
//  Copyright © 2020 IED. All rights reserved.
//

import UIKit

class CreaEventoController2 : UIViewController {
    
    // MARK: - Outlets
    
    @IBOutlet weak var textDescrizione: UITextView!
    
    
    // MARK: - Variabili locali
    
    var eventoDaCreare: CreaEvento?
    
    //MARK: - Setup
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        // Cambio il titolo della schermata
        navigationItem.title = "Crea evento"
        
        // Apro automaticamente la tastiera
        textDescrizione.becomeFirstResponder()
        
        // Riporto la descrizione inserita precedentemente dall'utente
        textDescrizione.text = eventoDaCreare?.descrizione
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        popolaEventoDaCreare()
    }
    
    private func popolaEventoDaCreare() {
        // Prendo la descrizione inserita dall'utente
        eventoDaCreare?.descrizione = textDescrizione.text
    }
}
