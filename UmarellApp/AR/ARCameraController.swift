//
//  ARCameraController.swift
//  UmarellApp
//
//  Created by iedstudent on 02/12/2019.
//  Copyright © 2019 IED. All rights reserved.
//

import UIKit

import ARKit

import SceneKit

class ARCameraController: UIViewController, ARSCNViewDelegate {
    
    // MARK: Outlets
    
    @IBOutlet weak var sceneView: SCNView!
    
    // MARK: - Setup
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        avviaSessione()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        interrompiSessione()
    }
    
    // MARK: - Funzioni private
    
    private func avviaSessione() {
        // Preparo la modalità AR
        let configuration = ARImageTrackingConfiguration()
        
        // Creo manualmente la reference image nel mondo reale
        let image = UIImage(named: "ImmagineDemo")!
        let referenceImage = ARReferenceImage(image.cgImage!, orientation: .up,
                                              physicalWidth: 0.05)
        
        // La aggiungo alla configurazione AR
        configuration.trackingImages = [referenceImage]
        
        // Avvio la modalità AR
        sceneView.delegate = self
        sceneView.session.run(configuration)
    }
    
    private func interrompiSessione() {
        // Interrompo la modalità AR
        sceneView.session.pause()
    }
    
    // MARK: - ARSCNView delegate
    
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor:
        ARAnchor) {
        
        print("Immagine trovata!")
        
        
        // Torno sul thread principale
        
        DispatchQueue.main.async {
            // Creo l'oggetto 3D del libro da aggiungere alla scena AR
            let scene = SCNScene(named: "book.scn")
            let node = scene?.rootNode.childNode(withName: "book", recursively: false)
            
            //Lo aggiungo alla scena
            node.addChildNode(bookNode!)
        }
        
    }
    
}
