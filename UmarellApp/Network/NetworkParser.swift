//
//  NetworkParser.swift
//  Balistreri
//
//  Created by IED Student on 10/06/2019.
//  Copyright © 2019 IED. All rights reserved.
//

import UIKit

class NetworkParser {
    
    static func parserMeteo(conData data: IEDDictionary) -> Meteo? {
        
        let meteo = Meteo()
        
        if let main = data["main"] as? IEDDictionary {
            meteo.temperatura = main["temp"] as? Double
        }
        
        if let weather = data["weather"] as? IEDArray {
            if let firstWeather = weather.first {
                meteo.descrizione = firstWeather["description"] as? String
            }
        }
        
        return meteo
    }
    
    static func parserUtente(conData data: IEDDictionary) -> Utente? {
        let utente = Utente()
        
        // traduzione dei dati inviati dal server
        
        utente.id = data["id_utente"] as? Int
        utente.authToken = data["auth_token"] as? String
        
        utente.email = data["email"] as? String
        utente.nome = data["nome"] as? String
        utente.avatarUrl = data["cognome"] as? String
        utente.email = data["email"] as? String
        utente.email = data["email"] as? String
        utente.email = data["email"] as? String
        
        //
        return utente
    }
    
    static func parseListaEventi(con array: IEDArray) -> [Evento] {
        // Parse degli N dictionary presenti nell'array JSON degli eventi
        for dictionary in array {
            let evento = parseEvento(con: dictionary)
            eventi.append(evento)
        }
        
        return eventi
    }
    
    static func parseEvento(con dictionary: IEDDictionary) -> Evento {
        let evento = Evento()
        
        // Parse degli attributi del dictionary
        evento.nome = dictionary["nome"] as? String
        
        if let dictionaryCreatore = dictionary["creatore"] as? IEDDictionary {
            evento.creatore = parserUtente(conData: dictionaryCreatore)
        }
        
        // Coordinate
        var coordinate = CLLocationCoordinate2D()
        coordinate.latitude = (dictionary["lat"] as? Double) ?? 0
        coordinate.longitude = (dictionary["lng"] as? Double) ?? 0
        
        return evento
    }
}
