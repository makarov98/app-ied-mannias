//
//  Network.swift
//  Balistreri
//
//  Created by IED Student on 10/06/2019.
//  Copyright © 2019 IED. All rights reserved.
//

import UIKit

class Network {
    
    static func richiestaMeteoRoma() {
        
        let url = "http://ied.apptoyou.it/app/meteoroma.php"
        
        IEDNetworking.jsonGet(url: url, authToken: nil, parameters: nil) {
            
            (risposta) in
            
            if risposta.success {
                
                // Controllo se i dati ricevuti sono del tipo che mi aspettavo
                if let temperatura = risposta.data as? Int {
                    
                    print("La temperatura a Roma è di \(temperatura) °C")
                }
            }
        }
        
    }
    
    
    typealias CompletitionMeteo = ((Meteo?) -> Void)
    
    static func richiestaMeteoEvento(_ evento: Evento?, completion: CompletitionMeteo?) {
        
        // Check validità dei dati
        guard let coordinate = evento?.coordinate else {
            // Dati non validi
            return
        }
        
        let url = "https://api.openweathermap.org/data/2.5/weather"
        
        // Parametri da passare al servizio
        var parametri = IEDDictionary()
        parametri["appid"] = "7854e283b3c65ba9943d850e002019b4"
        parametri["units"] = "metric"
        parametri["lang"] = "it"
        
        parametri["lat"] = coordinate.latitude
        parametri["lon"] = coordinate.longitude
        
        // Richiamiamo il servizio
        IEDNetworking.jsonGet(url: url, authToken: nil, parameters: parametri) {
            
            (risposta) in
            
            if risposta.success {
                
                if let dictionary = risposta.data as? IEDDictionary {
                    //Parse della risposta
                    let meteo = NetworkParser.parserMeteo(conData: dictionary)
                    
                    print("La temperatura è: \(meteo?.temperatura ?? 0)")
                    print("La descrizione è: \(meteo?.descrizione ?? "")")
                    
                    completion?(meteo)
                }
                
            }
            
        }
    }
    
    
    // MARK: Modifica Avatar
    
    typealias CompletionModificaAvatar = ((Bool) -> Void)
    
    static func richiestaModificaAvatarUtenteConnesso(nuovoAvatar: UIImage, completion: CompletionModificaAvatar?) {
        
        // Indirizzo del sito web da richiamare
        let url = "http://ied.apptoyou.it/app/modifica_avatar.php"
        
        // Creo il file dell'immagine da inviare al server
        let dataAvatar = UIImageJPEGRepresentation(nuovoAvatar, 1.0)!
        let fileAvatar = IEDNetworkingMultipartFile.init(parameter: "avatar", name: "avatar.jpg", mimeType: "image/jpg", data: dataAvatar)
        
        // Prendo il token dell'utente connesso
        let authToken = LoginUtility.utenteConnesso?.authToken
        
        // Invio la richiesta al server
        IEDNetworking.jsonMultipartPost(url: url, authToken: authToken, parameters: nil, multipartFiles: [fileAvatar]) {
            (response) in
            
            // Controllo se la richiesta è andata a buon fine
            if response.success {
                completion?(true)
            } else {
                // Si è verificato un errore
                completion?(false)
            }
        }
    }
    
    // MARK: Utente connesso
    
    typealias CompletionUtente = ((Utente?) -> Void)
    
    // Recupero le info aggiornate dell'utente connesso
    static func richiestaUtenteConnesso(completion: CompletionUtente?) {
        
        let url = "http://ied.apptoyou.it/app/utente.php"
        
    }
    
    // Richiesta per modificare le info dell'utente connesso
    static func richiestaModificaUtenteConnesso(_ utente: Utente, completition:
        CompletionUtente?) {
        
        // Indirizzo del servizio web da richiamare
        let url = "http://ied.apptoyou.it/app/modifica_utente.php"
        
        // Indirizzo del servizio web da richiamare
        let authToken = LoginUtility.utenteConnesso?.authToken
        
        // Creo il dictionary dei parametri da inviare al server
        var parameters = IEDDictionary()
        
        parameters["nome"] = utente.nome
        parameters["cognome"] = utente.cognome
        parameters["data_nascita"] = utente.dataNascita
        parameters["citta"] = utente.citta
        
        // Invio la richiesta al server
        IEDNetworking.jsonPost(url: url, authToken: authToken, parameters: parameters) {
            (risposta) in
            
            // Controllo se la chiamata è andata a buon fine
            if risposta.success {
                // Controllo se il server ha inviato i dati che aspettavo
                if let data = risposta.data as? IEDDictionary {
                    
                }
            }
        }
    }
    
    // MARK: - Eventi
    
    typealias CompletionListaEventi = (([Evento]?) -> Void)
    
    /// Richiesta per recuperare tutti gli eventi presenti sul database
    static func richiestaListaEventi(completion: CompletionListaEventi?) {
        
        // Indirizzo del servizio web da richiamare
        let url = "http://ied.apptoyou.it/app/api/eventi.php"
        
        IEDNetworking.jsonGet(url: url, authToken: nil, parameters: nil) {
            (risposta) in
            
            if risposta.success {
                // Evento creato correttamente
                completion? (nil)
            }
        }
    }
    
}

