//
//  ProfileController.swift
//  UmarellApp
//
//  Created by IED Student on 26/06/2019.
//  Copyright © 2019 IED. All rights reserved.
//

import UIKit

class ProfileController: UIViewController{
    
    @IBOutlet weak var EditButton: UIButton!
    @IBOutlet weak var labelNome: UILabel!
    @IBOutlet weak var labelCredito: UILabel!
    @IBOutlet weak var labelCitta: UILabel!
    @IBOutlet weak var labelData: UILabel!
    
    @IBOutlet weak var labelRegistratoDa: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        navigationItem.title = "Profilo"
        
        EditButton.layer.cornerRadius = EditButton.frame.size.height/2.0
        EditButton.layer.masksToBounds = true
        
        NetworkUtility.downloadImmagine(indirizzoWeb: "http://ied.apptoyou.it/app/avatars/default.jpg", perImageView: profileImage)
        labelNome.text = LoginUtility.utenteConnesso?.nome
        // labelRegistratoDa.text = LoginUtility.utenteConnesso?.timestamp
        
        labelCredito.text = LoginUtility.utenteConnesso?.email
    }
    
    
    //labelCredito.text = stringCredito
    
    // labelCitta.text = LoginUtility.utenteConnesso?.citta
    // labelData.text = LoginUtility.utenteConnesso?.dataDiNascita
    
    // MARK: - Actions
    
    override func viewDidAppear (_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        print("viewDidAppear")
        
        if let utenteConnesso = LoginUtility.utenteConnesso {
            configuraSchermata(con: utenteConnesso)
            aggiornaInfoUtenteConnesso()
            
        }
    }
    
    @IBAction func logOut(_ sender: Any) {
    }
    
    // Button modifica avatar
    
    @IBAction func buttonModificaAvatar(_ sender: Any) {
        
        //        // 1. Creo l'action sheet
        //        let actionSheet = UIAlertController.init(title: "Modifica avatar", message: "Da dove vuoi caricare l'avatar?", preferredStyle: .actionSheet)
        //
        //        // 2.a Creo l'azione del tasto "fotocamera"
        //        let actionFotocamera = UIAlertAction.init(title: "Fotocamera", style: .default) {
        //            (action) in
        //
        //            // L'utente ha scelto la fotocamera
        //            self.caricaAvatarDaFotocamera()
        //
        //            // 2.b Aggiungo l'azione all'action sheet
        //            actionSheet.addAction(actionFotocamera)
        //
        //            // 3a. Creo l'azione del tasto "galleria"
        //            let actionGalleria = UIAlertAction.init(title: "Galleria", style: .default) {
        //                (action) in
        //                // L'utente ha scelto la galleria
        //
        //                // 3b. Aggiungo l'azione all'action sheet
        //                actionSheet.addAction(actionGalleria)
        //
        //                // 4. Creo l'azione del tasto "annulla"
        //                let actionAnnulla = UIAlertAction(title: "Annulla", style: .cancel)
        //                actionSheet.addAction(actionAnnulla)
        //
        //                // 5. Mostro l'action sheet
        //                present(actionSheet, animated: true)
        //            }
        //
        //        }
    }
    
    
    // MARK: - Private functions
    
    private func configuraSchermata(con utente: Utente) {
        
        
    }
    
    private func aggiornaInfoUtenteConnesso() {
    
    }
    
}

