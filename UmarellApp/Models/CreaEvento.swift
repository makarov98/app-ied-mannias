//
//  CreaEvento.swift
//  UmarellApp
//
//  Created by iedstudent on 13/01/2020.
//  Copyright © 2020 IED. All rights reserved.
//

import UIKit
import CoreLocation

class CreaEvento {
    
    var nome: String?
    
    var descrizione: String?
    
    var prezzo: Double?
    
    var copertina: UIImage?
    
    var indirizzo: String?
    
    var data: String?
    
    /* override func viewDidLoad() {
     super.viewDiDLoad()
     
     let datePicker = UIDatePicker()
     datePicker.addTarget(self, action: #selector(datePickerModificato(_:)),
     for: .valueChanged)
     
     textData.inputView = datePicker
     } */
    
    @objc private func datePickerModificato(_ datePicker: UIDatePicker) {
        print(datePicker.date)
    }
}
