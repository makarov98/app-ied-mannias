//
//  Utente.swift
//  Balistreri
//
//  Created by ieduser on 08/04/2019.
//  Copyright © 2019 IED. All rights reserved.
//

import UIKit

/** Classe che definisce le proprietà e le azioni dell'entità "Utente" dentro la nostra app */

class Utente : NSObject, NSCoding {
    
    var id: Int?
    
    var authToken: String? // Aggiunto da Postman
    
    var nome: String?
    var cognome: String?
    
    var email: String?
    var password: String?
    
    var avatarUrl: String?
    var dataNascita: String?  // Aggiunto da Postman
    var citta: String?  // Aggiunto da Postman
    var credito: Double?  // Aggiunto da Postman
    
    // MARK: Protocollo NSCoding
    
    override init() {
        // Permette di inizializzare un oggetto utente anche senza una codificazione
    }
    
    func encode(with aCoder: NSCoder) {
        // Codifica
        aCoder.encode(id, forKey: "id")
        aCoder.encode(authToken, forKey: "authToken")
        aCoder.encode(nome, forKey: "nome")
        aCoder.encode(cognome, forKey: "cognome")
        aCoder.encode(email, forKey: "email")
        aCoder.encode(password, forKey: "password")
        aCoder.encode(avatarUrl, forKey: "avatarUrl")
        aCoder.encode(dataNascita, forKey: "dataNascita")
        aCoder.encode(citta, forKey: "citta")
        aCoder.encode(credito, forKey: "credito")
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        // Decodifica
        id = aDecoder.decodeObject(forKey: "id") as? Int
        authToken =  aDecoder.decodeObject(forKey: "authToken") as? String
        nome = aDecoder.decodeObject(forKey: "nome") as? String
        cognome = aDecoder.decodeObject(forKey: "cognome") as? String
        email = aDecoder.decodeObject(forKey: "email") as? String
        password = aDecoder.decodeObject(forKey: "password") as? String
        avatarUrl = aDecoder.decodeObject(forKey: "avatarUrl") as? String
        dataNascita = aDecoder.decodeObject(forKey: "dataNascita") as? String
        citta = aDecoder.decodeObject(forKey: "citta") as? String
        credito = aDecoder.decodeObject(forKey: "credito") as? Double
    }
    
}

extension Utente {
    /**
     var nomeCompleto: String?
     if let nome = nome, let cognome = cognome {
     return nome + " " + cognome
     }
     else if let nome = nome {
     return nome
     }
     
     return nil
     */
}
