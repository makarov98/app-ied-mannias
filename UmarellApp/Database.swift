//
//  Database.swift
//  Balistreri
//
//  Created by IED Student on 27/05/2019.
//  Copyright © 2019 IED. All rights reserved.
//

import UIKit
import MapKit

class Database {
    // lista eventi raggiungibili in qualsiasi punto dell'app
    static var eventi:[Evento] = []
    
    static func creaEventiDiProva() {
        let eventoUno = Evento()
        eventoUno.creatore = LoginUtility.utenteConnesso
        eventoUno.nome = "Lezione App Design"
        eventoUno.descrizione = "Adoroh"
        eventoUno.indirizzo = "Via Alcamo 11, Rome, Italia"
        eventoUno.data = Date()
        eventoUno.prezzo = 55
        eventoUno.copertinaUrl = "http://images.unsplash.com/photo-1489001606409-a7fd2655b8da?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80"
        
        // assegno le coordinate
        eventoUno.coordinate = CLLocationCoordinate2D.init(latitude: 41.8864837, longitude: 12.523872)
        
        // creo un oggetto acquistabile di prova
        let acquistabileUno = OggettoAcquistabile()
        acquistabileUno.nome = "Maglia"
        acquistabileUno.quantità = 1
        acquistabileUno.prezzo = 10
        acquistabileUno.immagineUrl = "http://images.unsplash.com/photo-1508963493744-76fce69379c0?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80"
        
        //lo aggiungiamo agli oggetti acquistabili
        eventoUno.oggettiAcquistabili = [acquistabileUno]
        
        //lo aggiungiamo alla lista
        eventi.append(eventoUno)
        
        let eventoDue = Evento()
        eventoDue.creatore = LoginUtility.utenteConnesso
        eventoDue.nome = "Voragine a San Giovanni"
        eventoDue.indirizzo = "Via Leopardi 10, CB, Italia"
        eventoDue.data = Date()
        eventoDue.prezzo = 55.0
        eventoDue.copertinaUrl = "http://images.unsplash.com/photo-1465493251445-c6af8fc40b7a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1353&q=80"
        
        // assegno le coordinate
        eventoDue.coordinate = CLLocationCoordinate2D.init(latitude: 45.8864837, longitude: 23.523872)
        
        //lo aggiungiamo alla lista
        eventi.append(eventoDue)
        
        let eventoTre = Evento()
        eventoTre.creatore = LoginUtility.utenteConnesso
        eventoTre.nome = "Albero Caduto a San Marco"
        eventoTre.indirizzo = "Corso Vittorio, CB, Italia"
        eventoTre.data = Date()
        eventoTre.prezzo = 55.0
        eventoTre.copertinaUrl = "http://images.unsplash.com/photo-1465493251445-c6af8fc40b7a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1353&q=80"
        
        // assegno le coordinate
        eventoTre.coordinate = CLLocationCoordinate2D.init(latitude: 41.8864837, longitude: 12.523872)
        
        //lo aggiungiamo alla lista
        eventi.append(eventoTre)
    }
}
