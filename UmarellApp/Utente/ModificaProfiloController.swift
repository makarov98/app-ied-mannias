//
//  ModificaProfiloController.swift
//  UmarellApp
//
//  Created by iedstudent on 25/11/2019.
//  Copyright © 2019 IED. All rights reserved.
//

import UIKit

class ModificaProfiloController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Actions
    
    @objc private func buttonAnnulla() {
        //Chiudo la schermata
        dismiss(animated: true)
    }
    
    @objc private func nuttonSalva() {
        guard let utente = LoginUtility.utenteConnesso else {
            return
        }
        
        // 1. Prendo i dati aggiornati presenti sui campi di testo
        
        // CODICE DA SCRIVERE
        // ...
        // ...
        // ...
        // ...
        
        // 2. Invio al server l'utente con i dati aggiornati
        Network.richiestaModificaUtenteConnesso(utente) {
            (utenteAggiornato) in
            
            // Controllo se il server ha risposto correttamente
            
            if let utenteAggiornato = utenteAggiornato {
                // faccio un backup dell'authToken dell'utente
                
                utente.authToken = LoginUtility.utenteConnesso?.authToken
                
                // Salvo l'utente aggiornato sul database
                
                LoginUtility.utenteConnesso = utenteAggiornato
                LoginUtility.salva()
                
                // Chiudo la schermata di modifica profilo
                self.dismiss(animated: true)
            } else {
                // Nessun utente ricevuto dal server
                AlertUtility.mostraAlertSemplice(titolo: "Si è verificato un errore", messaggio: nil, viewController: self)
            }
        }
    }
    
}
