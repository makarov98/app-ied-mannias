//
//  LoginUtility.swift
//  Balistreri
//
//  Created by ieduser on 08/04/2019.
//  Copyright © 2019 IED. All rights reserved.
//

import UIKit

class LoginUtility {
    /** Classe che semplifica l'interazione con il database per capire se c'è un utente connesso all'app**/
    
    // L'eventuale utente connesso
    static var utenteConnesso : Utente?
    
    // La chiave per salvare/caricare l'utente nel database
    private static let ChiaveUtenteConnesso = "KeyUtenteConnesso"
    
    // Funzione per salvare l'utente connesso sul database dell'app
    static func salva() {
        
        if let utente = utenteConnesso {
            // C'è un utente connesso da salvare
            // Converto l'oggetto Utente in un insieme di byte "Data"
            let dataUtente = NSKeyedArchiver.archivedData(withRootObject: utente)
            
            UserDefaults.standard.set(dataUtente, forKey: ChiaveUtenteConnesso)
        }
        
        // Comunico al sistema di scrivere sul database
        UserDefaults.standard.synchronize()
    }
    
        //funzione per leggere l'utente connesso sul database dell'app
    static func carica() {
        
        let oggettoSalvato = UserDefaults.standard.object(forKey: ChiaveUtenteConnesso)
        
        if let dataUtente = oggettoSalvato as? Data {
            // Ci sono dati di un utente connesso
            
            // Converto i byte "Data" in un oggetto "Utente"
            let utente = NSKeyedUnarchiver.unarchiveObject(with: dataUtente) as? Utente
            
            // Lo salvo in sessione
            utenteConnesso = utente
            
        }
        UserDefaults.standard.object(forKey: ChiaveUtenteConnesso)
    }
}
